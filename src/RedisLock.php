<?php

namespace Shifeng\Redislock;

/**
 * redis分布式锁
 */
class RedisLock
{
    protected $redis;

    protected $lockValue;

    protected static $redisLockObj = null;

    /**
     * @param $redis \Redis
     */
    private function __construct($redis = null)
    {
        $this->redis = $redis;
    }

    /**
     * 禁止克隆
     */
    private function __clone()
    {
    }

    /**
     * 禁止重构
     */
    private function __wakeup()
    {
    }

    /**
     * @return RedisLock|null
     */
    public static function getInstance($redis)
    {
        if (self::$redisLockObj == null) {
            self::$redisLockObj = new RedisLock($redis);
        }
        return self::$redisLockObj;
    }

    /**
     * @param $key 加锁的key
     * @param int $expire 锁的过期时间，避免死锁
     * @param int $retry 尝试次数，如果锁被占用，尝试5次加锁
     * @param int $sleep 重试尝试需要等待的时间
     */
    public function lock($key, $expire = 5, $retry = 5, $sleep = 1000)
    {
        $res = false;
        $value = time() . mt_rand();
        while ($retry-- > 0) {
            $res = $this->redis->set($key, $value, ['nx', 'ex' => $expire]);
            if ($res) {
                $this->lockValue[$key] = $value;
                //加锁成功，跳出循环
                break;
            }

            usleep($sleep);  //休眠后尝试重试获取锁
        }
        return $res;
    }

    /**
     * 解锁
     * @param $key
     */
    public function unlock($key)
    {
        //使用lua脚本解锁
        $script = 'if redis.call("get",KEYS[1]) == ARGV[1]
                    then
                        return redis.call("del",KEYS[1])
                    else
                        return 0
                    end ';

        if (isset($this->lockValue[$key])) {
            $this->redis->eval($script, [$key, $this->lockValue[$key]], 1);
        }
    }
}
